"""khan-thugad.bot.views module."""
from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

from linebot import LineBotApi, WebhookParser
from linebot.exceptions import InvalidSignatureError, LineBotApiError

from .handlers import (
    EchoHandler, SearchHandler
)

LINE_BOT_API = LineBotApi(settings.LINE_CHANNEL_ACCESS_TOKEN)
PARSER = WebhookParser(settings.LINE_CHANNEL_SECRET)


@csrf_exempt
def index(request):
    """index method.

    :param request request: request yang diterima.
    """
    return HttpResponse('')


@csrf_exempt
def callback(request):
    """callback method.

    :param request request: request yang diterima.
    """
    methods = (
        SearchHandler(), EchoHandler(),
    )
    if request.method == 'POST':
        signature = request.META['HTTP_X_LINE_SIGNATURE']
        body = request.body.decode('utf-8')

        try:
            events = PARSER.parse(body, signature)
        except InvalidSignatureError:
            return HttpResponseForbidden()
        except LineBotApiError:
            return HttpResponseBadRequest()

        for event in events:
            for method in methods:
                if method.is_it_for_me(event):
                    print(method)
                    messages = method.process(event)
                    LINE_BOT_API.reply_message(event.reply_token, messages)
                    break
        return HttpResponse()
    return HttpResponseBadRequest()
