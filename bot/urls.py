"""khan-thugad.bot.urls module."""
from django.urls import path
from .views import callback, index

urlpatterns = [
    path('', index, name='index'),
    path('callback/', callback, name='callback'),
]
