"""khan-thugad.bot.urls module."""
import json
import re
import urllib

from django.conf import settings

from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage
)

NO_MOV_MSG = (
    'Movie not found.\n'
    'Try something else.'
)

class SearchHandler:
    """SearchHandler

    Class SearchHandler untuk membalas pesan dengan review film.
    """
    def is_it_for_me(self, event):
        """is_it_for_me method.

        :param Event event: Event yang akan ditangkap.
        """
        if isinstance(event, MessageEvent):
            if isinstance(event.message, TextMessage):
                text = event.message.text
                if re.match('..search', text, re.M|re.I):
                    return True
        return False

    def process(self, event):
        """process method.

        :param Event event: Event yang akan ditangkap.
        """
        if isinstance(event, MessageEvent):
            if isinstance(event.message, TextMessage):
                text = event.message.text
                if re.match(r'^..search', text, re.M|re.I):
                    query = re.search(r'(?<=^..search )(((\w|\.)+)( ))*((\w|\.)+)', text).group()
                    data = self.get_query_json(query)
                    keys = data.keys()
                    text = ""
                    if keys:
                        for key in keys:
                            text = self.get_text_json(key)['text']
                            text = text[0:2000]
                            break
                    else:
                        text = NO_MOV_MSG
                    return TextSendMessage(text=text)

    def get_query_json(self, query):
        """get_query_json method.

        :param str query: String dari query.
        """
        query = ('%20').join(query.split(' '))
        url = 'api/get-query/{query_str}'.format(
            query_str=query,
        )
        url = settings.ROOT_URL + url
        print(url)
        read_data = urllib.request.urlopen(url).read()
        data = json.loads(read_data)
        return data

    def get_text_json(self, doc_id):
        """get_query_json method.

        :param str query: String dari query.
        """
        url = 'api/get-text/{doc_id}'.format(
            doc_id=doc_id,
        )
        url = settings.ROOT_URL + url
        print(url)
        read_data = urllib.request.urlopen(url).read()
        data = json.loads(read_data)
        return data
