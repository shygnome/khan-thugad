"""khan-thugad.bot.urls module."""
import re

from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage
)

NO_CMD_MSG = (
    'Command not found.\n'
    'Try "..help".'
)
HELP_MSG = (
    "All command started with '..' "
    "followed by what you want to do.\n\n"
    "The full list of command is as follows :\n\n"
    "# Main Features\n\n"
    "'..search [criteria to search]' - search for a movie "
    "review based on criteria.\n"
    "end of file"
)


class EchoHandler:
    """EchoHandler

    Class EchoHandler untuk echo pesan.
    """
    def is_it_for_me(self, event):
        """is_it_for_me method.

        :param Event event: Event yang akan ditangkap.
        """
        if isinstance(event, MessageEvent):
            if isinstance(event.message, TextMessage):
                return True
        return False

    def process(self, event):
        """process method.

        :param Event event: Event yang akan ditangkap.
        """
        if isinstance(event, MessageEvent):
            if isinstance(event.message, TextMessage):
                text = event.message.text
                if re.match(r'^..help$', text, re.M|re.I):
                    return TextSendMessage(text=HELP_MSG)
                return TextSendMessage(text=NO_CMD_MSG)
