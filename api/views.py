"""khan-thugad.api.views module."""
import os

from django.http import JsonResponse

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize

FILE_DIR = os.path.dirname(__file__)
DUMMY_CORPUS_TITLE = {
    1 : "Kedarnath",
    2 : "2.0",
    3 : "The Dark Side Of Life: Mumbai City",
    4 : "Bhaiaji Superhit",
    5 : "Hotel Milan",
}
DUMMY_CORPUS_TEXT = {
    1 : "Kedarnath features an all-too-familiar love story, that gets a boost thanks to the climactic floods that devastated civilization in the mountain town of Uttarakhand, in 2013. The screenplay has its strengths and weaknesses, but through the crest and trough of dramatic waves, what stands out as a solid force is young debutante Sara Ali Khan. Her first performance on celluloid has the right amount of fire to spark a connection with the audience.The film is based in the valleys and mountain ranges of Kedarnath and the drone-assisted cinematography by Tushar Kanti Ray, which explores the picturesque settings, is impressive. The film captures the beauty of the Himalayas with crisp and artistic visuals. There’s also the quaint, small-town setting, where locals treat Hindu pilgrims with utmost respect and where Muslim porters, have no qualms in praying to Shiva and sharing the faith of the devotees. The writing by Abhishek Kapoor and Kanika Dhillon, subtly reflects upon the secular dynamics of Kedarnath. There’s also a brief comment on the commercialisation with hotels, malls and tourism, which has impacted the ecological balance of places like Kedarnath and contributed to natural calamities. These pertinent issues, which had a lot of potential, are briefly touched upon and then left in deep waters.The focus remains strictly on the love story, and while Mukku and Mansoor share some beautiful moments, the narrative does take a tad too long to set up their romance. What makes up for the slow pace of the film are the performances and the CGI-driven climactic portions. They come together to create a strong, dramatic impact. Sara Ali Khan as Mukku is a live wire. She reminds you of a young and boisterous Amrita Singh (her mother) in films like Betaab and Chameli Ki Shaadi. Her confidence and on-screen charm are a testament to her ability and talent. She looks gorgeous in the desi-girl avatars and she owns every scene that she’s present in. Sushant Singh, in a slightly under-played role, supports Sara’s efforts, but we’ve seen him give finer performances before. Watch out for the scene where he sings Lata Mangeshkar’s Lag Ja Gale (from Woh Kaun Thi, 1964) where his character’s naivety is brought out with deftness.For a love story, there are no romantic tracks that really hold your attention. Apart from the song Namo Namo, Amit Trivedi’s music doesn’t create the required mood for a love saga like Kedarnath. Director Abhishek Kapoor’s attempt to make a film set against the backdrop of a natural calamity of this proportion is ambitious and sincere. The setting is what makes a difference, because the simple and predictable love story doesn’t really rose above the water level. The clever use of CGI mixed with live-action shots to depict moments like cloudburst and the pilgrim city submerging underwater creates an authentic impact. The execution in these areas, along with a memorable debut by Sara, is what keeps the film afloat.Also See: Bollywood celebs floods Twitter with praises for Sushant Singh Rajput and Sara Ali Khan starrer 'Kedarnath'",
    2 : "South films and filmmakers seem to have a handle on out-of-the-box ideas for cinema. Director Shankar has dabbled with such high-concept movies like Robot (2010, Tamil title Enthiran) and I (2015) before, too. While he’s no stranger to depicting far-fetched and creative ideas, the unique story of 2.0 does feel a bit underwhelming, especially in the wake of the spectacle created by the CGI-driven execution. The slick visual presentation though, more than makes up for what the story lacks. The twists and surprises of 2.0 are predictable and the fact that the story follows the template set by the first film, doesn't really add to the experience. But Shankar’s vision and his team’s technical wizardry with the visual effects creates a positive impact. Akshay Kumar’s shape-shifting antagonist Pakshi, made up of millions of cellphones is impressive to say the least. There are several moments in the movie that are quite memorable too. The sight of mobiles covering up an entire road, a forest and forming the giant eagle, are most certainly ‘wow’ worthy. But the scientific explanations provided for telekinetic control of mobile phones and the antagonist’s supernatural abilities aren't all that convincing. Nevertheless, this is a Rajinikanth movie and questioning cinematic liberty and logic-defying creativity is unwarranted.Rajinikanth’s performance and presence dominates the narrative. Shankar’s screenplay gives fans of the superstar plenty of moments to cheer for, especially with the fact that there are not one or two, but three iterations of his robotic character Chitti. Not just that, even the one-liners in the movie and the reference to Rajini’s numero uno status gives the fans plenty to cheer for. Akshay Kumar also gives a solid performance as the antagonist, especially in the flashback that reveals his character’s ornithology background. The presence of these two superstars is one of the main reasons why 2.0 feels like a grand, big-ticket movie.The makeup and costumes are highlights of the movie, too. The effort to create Akshay’s character is really detailed you don’t need to be a VFX guru to realise that the characters of 2.0 really look authentic. The background score by AR Rahman and Qutub-E-Kripa also adds to feel of this futuristic film. While the movie certainly looks world-class, the writing doesn’t always live up to the expectations. It’s Rajinikanth and Akshay Kumar’s star power that really saves the day for this magnum opus.",
    3 : "Seven individual lives, all intertwined in ways more than one, are grasped in the clutches of life’s inexorable difficulties that ultimately pushes them over the fence, prompting almost all the characters – Anand (Nikhil Ratnaparkhi), Parul (Jyoti Malshe), Kavya (Neha Khan), Warren Lobo (Deep Raj Rana),Sumit Balsaria (Kay Kay Menon), Zulfiqar Hussain (Mahesh Bhatt), Prince (Avii) – to succumb to the blows of depression. ‘The Dark Side of Life: Mumbai City’ is an attempt to portray the shadowy side of a big city like Mumbai and how, in order to blend in with the crowd and live up to their sky-high expectations, people make irreversible mistakes before the realisation sets in that life in a concrete jungle is not as rosy as it seems. The incidents shown in this slice-of-life flick, which are supposedly grim and a reflection of how life looks like, does not resonate with the audience whatsoever. Coupled with a lacklustre script, is some bad acting and shoddy cinematography. Mahesh Bhatt, who plays the role of a pining father, is quite the natural in front of the camera. Kay Kay Menon, despite a weak story in hand, renders a performance that one would expect from a seasoned actor like him. Rest of the cast was pretty bland and failed to evoke any sense of grief or pity for the stories they are a part of and the characters they essay on screen. ‘The Dark Side of Life: Mumbai City’ has many enemies that kill its honest attempt towards highlighting the alarming issue of depression and suicide, and the length of it is one of them. The reiterating theme song in the film, Aawargi, is used as and when pleased, which is a big downer. And, the treatment of the sub-plots fail to justify each character’sdecision to end their lives and the subsequent change of heart, especially because their grief doesn’t translate into the screen the way it should have. If you are new to Mumbai, this movie is perhaps a poor option to turn to for understanding the pulse of this vivacious city. ‘The Dark Side of Life: Mumbai City’ is chaotic, haphazard. The drama is a sum total of everything and that's where it goes completely wrong.",
    4 : "Lal Bhaisahab Dubey aka 3D (Sunny Deol) is Varanasi’s most feared crime lord. While he lords over all, his wife Sapna Dubey (Preity Zinta), the daughter of another crime boss, bosses over his heart. Out of a fit of jealousy she leaves him and now he has become a crying wreck, which is bad for his personal and professional life. At the same time, there is the evil Helicopter Mishra (Jaideep Ahlawat) who is looking to win the title of the number one don in the city. With all of this in play, 3D somehow thinks of making a film and becoming popular, which, he thinks, will impress Sapna and have her return to him. For this, he hires a crafty Bollywood director Goldie Kapoor (Arshad Warsi), a meek writer Tarun Porno Ghosh (Shreyas Talpade) and the hot superstar Mallika (Ameesha Patel) to make the film titled ‘Bhaiaji Superhit’. To cut a long review short, this is Sunny Deol’s ‘Dabangg’, but not half as entertaining. The first 10 minutes has some genuinely clever lines and funny moments, but after that it gets tiresome and overburdened with too much plot that the filmmakers have tried to pack together. The second half is where you get all the money shots, but that’s too late. The film would have been fun had the makers stuck to the interesting plot points and cut down the extra flab, of which there is a lot of, in the film. Sunny Deol’s dhai kilo ka haath from the 90s has become dhai ton ka haath in 2018, as he knocks down walls, bends thick iron rods as he beats the goons silly. He literally uses his fists like Thor uses his hammer. In fact, this movie should have been listed in the superhero genre, because Sunny paaji is literally cast as hulk. The only superpower he lacks is flight.While Sunny Deol and Arshad Warsi play their roles well, Ameesha Patel and Shreyas Talpade don’t quite do justice to theirs. Jaideep Ahlwawat and Sanjay Mishra don’t have much to do here either. Preity Zinta, as the feisty wife, too, does justice to her role.If over the top spectacle’s which defy logic, gravity and other rules are your cup of tea, this one’s for you. For the rest, this one is best enjoyed sitting in the barber shop as it plays on the television behind you.",
    5 : "Vipul’s ever-nagging girlfriend Shaheen (Karishma Sharma) dumps him for poking his nose where it doesn’t belong, that is trying to dissuade the Aam Bhakt Dal from harassing young couples. Soon enough, Vipul and Saurabh trick the latter’s uncle (Zakir Hussain) in to letting his old, abandoned lodge under the pretence that they’ll help him earn lakhs of rupees if he lets them rent out rooms on an hourly basis for couples to ‘talk’. Taking a pot-shot at a certain Indian minister, who has introduced an anti-Romeo squad for prevention of eve-teasing outside schools/colleges and general protection of women, ‘Hotel Milan’ tries to show the ill-effects of having such a moral-policing group. The film also brushes upon the bigger subject of why consensual sex between adults is still frowned upon in soceity. Other than the novelty of the central plot, nothing really works in favour of director Vishal Mishra’s ‘Hotel Milan’. Kunaal Roy Kapur as the protagonist is a misfit, who has not even finished the entire dub for his role, struggles to ace the Kanpur accent and as well as the nuances of a small-town chap. Secondly, Karishma Sharma as Shaheen is outrageously overdressed for a film based out of a tier-two city. The only saving grace of this purported satire is the sidekick Saurabh, played by Zeishan Quadri. The only time you will crack up watching this social drama-comedy is when he shows up on screen, the rest of the film seems as shady as a love hotel.",
}

DUMMY_INV_INDEX = {
    "test" : {
        1, 2, 3, 4,
    },
    "dark" : {
        3,
    },
    "bhaiaji" : {
        4,
    },
    "2.0" : {
        2,
    },
    "hotel" : {
        5,
    }
}

def index(request):
    """index method.

    :param request request: request yang diterima.
    """
    response = {}
    return JsonResponse(response, safe=False)


def get_query(request, query_str):
    """get_query method.

    :param request request: request yang diterima.
    :param str query_str: string dari query.
    """
    text = normalize(query_str)
    words = word_tokenize(text)
    list = []

    for word in words:
        temp = single_query(word)
        list.append(temp)
    docs = set.union(*list)
    response = {}
    for doc in docs:
        temp = {
            "title" : DUMMY_CORPUS_TITLE[doc],
        }
        response[doc] = temp
    return JsonResponse(response)


def get_text(request, doc_id):
    """get_text method.

    :param request request: request yang diterima.
    :param int id: id dari dokumen yang ingin diambil.
    """
    text = DUMMY_CORPUS_TEXT[doc_id]
    response = {
        "text" : text,
    }
    return JsonResponse(response)

def single_query(query_str):
    """single_query method.

    :param str query_str: string dari query.
    """
    if query_str in DUMMY_INV_INDEX:
        docs = DUMMY_INV_INDEX[query_str]
    else:
        docs = set()
    return docs

def normalize(text):
    """normalize method.

    :param str text: string dari teks yang ingin dinormalisasi.
    """
    stop_words = set(stopwords.words('english'))
    words = word_tokenize(text.lower())
    ps = PorterStemmer()
    result = ""

    for word in words:
        if not word in stop_words:
            result += " " + ps.stem(word)

    return result.strip()
