"""khan-thugad.bot.urls module."""
from django.urls import path
from .views import index, get_query, get_text

urlpatterns = [
    path('', index, name='index'),
    path('get-query/<str:query_str>', get_query, name='get-query'),
    path('get-text/<int:doc_id>', get_text, name='get-text'),
    # path('callback/', callback, name='callback'),
]
