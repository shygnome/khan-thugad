"""khan-thugad.api.reviews.summarizer module."""

import json
import os

from collections import defaultdict
from heapq import nlargest
from string import punctuation

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.probability import FreqDist


FILE_DIR = os.path.dirname(__file__)

def main():
    """
    main method.
    """
    path = os.path.join(FILE_DIR, 'corpus/doc-to-review.json')

    with open(path) as file:
        data = json.load(file)

    reviews = data["reviews"]
    summaries = []

    for review in reviews:
        content = review["text"]
        content = sanitize_input(content)
        sentence_tokens, word_tokens = tokenize_content(content)  
        sentence_ranks = score_tokens(word_tokens, sentence_tokens)
        summary = summarize(sentence_ranks, sentence_tokens, 4)
        temp = {
            "id" : review["id"],
            "text" : summary
        }
        summaries.append(temp)

    path = os.path.join(FILE_DIR, 'corpus/doc-to-review-summary.json')
    with open(path, 'w', encoding='utf8') as file:
        json.dump(summaries, file, indent=4, ensure_ascii=False)


def sanitize_input(data):
    """
    Menghilangkan whitespace.
    """
    replace = {
        ord('\f') : ' ',
        ord('\t') : ' ',
        ord('\n') : ' ',
        ord('\r') : None
    }

    return data.translate(replace)

def tokenize_content(content):
    """
    Melakukan tokenisasi, lowercase dan membuang stopword.
    """
    stop_words = set(stopwords.words('english') + list(punctuation))
    words = word_tokenize(content.lower())

    return [
        sent_tokenize(content),
        [word for word in words if word not in stop_words]    
    ]

def score_tokens(filterd_words, sentence_tokens):
    """
    Membangun frequency map untuk membangun rangking dari kalimat (scoring).
    """
    word_freq = FreqDist(filterd_words)

    ranking = defaultdict(int)

    for i, sentence in enumerate(sentence_tokens):
        for word in word_tokenize(sentence.lower()):
            if word in word_freq:
                ranking[i] += word_freq[word]

    return ranking

def summarize(ranks, sentences, length):
    """
    Memilih N kalimat tertinggi berdasarkan perhitungan rangking.
    """
    if int(length) > len(sentences): 
        print("Kalimat yang tersedia tidak cukup.")
        exit()

    indexes = nlargest(length, ranks, key=ranks.get)
    final_sentences = [sentences[j] for j in sorted(indexes)]
    return ' '.join(final_sentences) 

if __name__ == "__main__":
    main()
