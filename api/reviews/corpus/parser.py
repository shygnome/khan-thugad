import io
from bs4 import BeautifulSoup
from selenium import webdriver
import json


def getReview(link):
    driver = webdriver.Chrome("C:/Users/Andrew/Downloads/chromedriver_win32/chromedriver.exe")
    driver.get(link)
    html = driver.page_source
    soup = BeautifulSoup(html, "html.parser")
    soup.encode()
    filmDict = {}

    breadcrum = soup.find("div",{"class","md_topband clearfix"})
    h2Text = breadcrum.find("h2").getText()
    title = h2Text.split(" ")
    del title[-1]
    title = " ".join(title)

    story = soup.find('div',{'class','Normal'}).getText()
    if story == '\n':
        story = soup.find('div',{'class','Normal'})
        story = story.find('b').next_sibling
        if story == '\n':
            story = soup.find('div',{'class','Normal'})
            story = story.find('strong').next_sibling
    
    temp = story.split(" ")
    review = []
    flag = False
    for i in temp:
        if i == "Review:" or i == "REVIEW:":
            flag = True
        elif flag == True:
            review.append(i)
    res = " ".join(review)
    filmDict[title] = res
    return filmDict

lines = [str(line.rstrip('\n')) for line in open('film list.txt')]
f = open("outputTitle.txt", "w")
g = open("outputReview.txt", "w")
dictUrl = dict()
cnt = 1
for j in range(len(lines)):
    docs = getReview(lines[j])
    key = docs.keys()
    key = " ".join(key)
    value = docs.values()
    value = " ".join(value)
    f.write(str(cnt))
    f.write(" --> ")
    f.write(key)
    f.write("\n")
    g.write(str(cnt))
    g.write(" --> ")
    g.write(value)
    g.write("\n")
    cnt=cnt+1
    # dictUrl.update(docs)


# jsondump = json.dumps(dictUrl, ensure_ascii=True, indent=4)
# with open("palol.json", "w", encoding="utf8") as writer:
#     lineDump = jsondump.split("\\n")
#     for i in lineDump:
#         writer.writelines(i)
# print(getReview("https://timesofindia.indiatimes.com/entertainment/hindi/movie-reviews/hotel-milan/movie-review/66684687.cms"))